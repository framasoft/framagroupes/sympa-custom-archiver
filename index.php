<?php
    $config = yaml_parse_file('/etc/custom_archiver.yml');
    $pguser = $config['db']['user'];
    $pgdb   = $config['db']['database'];
    $pgpass = $config['db']['password'];

    $dbconn = pg_connect("host=127.0.0.1 dbname=$pgdb user=$pguser password=$pgpass options='--client_encoding=UTF8'");

    $title = 'Framagroupes custom archiver';
    if (isset($_GET['list']) && $_GET['list'] !== '') {
        $res = pg_prepare($dbconn, 'select_mails_from_list', 'SELECT m.* FROM mails m JOIN lists l ON l.id = m.list_id WHERE l.name = $1 ORDER BY m.date ASC');
        $res = pg_execute($dbconn, 'select_mails_from_list', [$_GET['list']]);
        $count = pg_num_rows($res);

        if ($res) {
            if ($count > 1) {
                $title = "$count mails in list " . $_GET['list'];
            } else {
                $title = "$count mail in list " . $_GET['list'];
            }
        }
    } else if (isset($_POST['message-id']) && $_POST['message-id'] !== '') {
        header('content-type: application/json; charset=utf-8');
        if (isset($config['reporting_spam_script_path'])) {
            $res = pg_prepare($dbconn, 'select_mail_from_mid', 'SELECT path FROM mails WHERE message_id = $1');
            $res = pg_execute($dbconn, 'select_mail_from_mid', [$_POST['message-id']]);
            $path = pg_fetch_row($sth)[0];
            $output = null;
            $retval = null;
            $passed = exec("cat $path | " . $config['reporting_spam_script_path'], $output, $retval);
            if ($passed) {
                if (unlink($path)) {
                    pg_prepare($dbh, 'delete_old_mail', 'DELETE FROM mails where message_id = $1');
                    pg_execute($dbh, 'delete_old_mail', [$_POST['message-id']]);
                    echo json_encode([
                        'success' => true,
                        'output'  => 'Mail successfully passed to reporting spam script and deleted.'
                    ]);
                } else {
                    echo json_encode([
                        'success' => false,
                        'output'  => "Mail successfully passed to reporting spam script but $path was not deleted."
                    ]);
                }
            } else {
                echo json_encode([
                    'success' => false,
                    'output'  => "$output\nReturn code: $retval"
                ]);
            }
        } else {
            echo json_encode([
                'success' => false,
                'output'  => 'The reporting_spam_script_path parameter is not set in config.'
            ]);
        }
        return;
    } else {
        $res = pg_query($dbconn, 'SELECT id, name FROM lists ORDER BY name ASC');
        $count = pg_num_rows($res);
        if ($count > 1) {
            $title = "Framagroupes custom archiver ($count lists archived)";
        } else {
            $title = "Framagroupes custom archiver ($count list archived)";
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title><?= $title ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link href="css/sca.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <main role="main">
            <h1><?= $title ?></h1>
                <?php if (isset($_GET['list']) && $_GET['list'] !== '') { ?><div class="modal" id="modal">
                    <div class="modal-bg modal-exit"></div>
                    <div class="modal-container">
                        <h1 id="messageid"></h1>
                        <dl>
                            <dt>Date</dt>
                            <dd id="date"></dd>
                            <dt>Subject</dt>
                            <dd id="subject"></dd>
                            <dt>From</dt>
                            <dd id="from"></dd>
                            <dt>Text</dt>
                            <dd id="text"></dd>
                            <dt>Html</dt>
                            <dd id="html"></dd>
                            <dt>Raw headers</dt>
                            <dd id="headers"></dd>
                        </dl>
                        <button class="modal-close modal-exit">X</button>
                    </div>
                </div>
                <section>
                    <a href="index.php">Go back to lists</a>
                </section>
                <section>
                    <form>
                        <label for="mid">Filtrer par Message-ID</label>
                        <input name="mid" id="mid" autofocus>
                    </form>
                </section>
                <section id="mails">
                    <table>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Subject</th>
                                <th>From</th>
                                <th>Message-ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($row = pg_fetch_assoc($res)) { $a = '<a href="#" data-modal="modal"
                                data-messageid="' . htmlspecialchars(htmlspecialchars($row['message_id'])) . '"
                                data-date="' . htmlspecialchars(htmlspecialchars($row['date'])) . '"
                                data-subject="' . htmlspecialchars(htmlspecialchars($row['subject'])) . '"
                                data-from="' . htmlspecialchars(htmlspecialchars($row['mailfrom'])) . '"
                                data-text="' . nl2br(htmlspecialchars(htmlspecialchars($row['body']))) . '"
                                data-html="' . nl2br(htmlspecialchars(htmlspecialchars($row['html']))) . '"
                                data-headers="' . nl2br(htmlspecialchars(htmlspecialchars($row['headers']))) . '">' ?>
                                <tr>
                                    <td><?= $a . htmlspecialchars($row['date']) ?></a></td>
                                    <td><?= $a . htmlspecialchars($row['subject']) ?></a></td>
                                    <td><?= $a . htmlspecialchars($row['mailfrom']) ?></a></td>
                                    <td class="message_id"><?= $a . htmlspecialchars($row['message_id']) ?></a></td>
                                </tr>
                            </a>
                        <?php } ?></tbody>
                    </table>
                <?php } else { ?><section>
                    <form method="GET" action="index.php">
                        <label for="list">Liste à consulter</label>
                        <input name="list" id="list" autofocus>
                        <button type="submit">Consulter</button>
                    </form>
                </section>
                <section id="lists">
                    <ul>
                    <?php while ($row = pg_fetch_assoc($res)) { ?>    <li>
                            <a href="index.php?list=<?= urlencode($row['name']) ?>"><?= htmlspecialchars($row['name']) ?></a>
                        </li>
                    <?php } ?></ul>
            <?php } ?></section>
            </main>
            <footer>
                <p>
                    <small><a href="https://framagit.org/framasoft/framagroupes/sympa-custom-archiver" title="Dépôt Git de cette page">Sympa custom archiver, AGPLv3</a></small>
                </p>
            </footer>
        </div>
        <script>
            const modals = document.querySelectorAll('[data-modal]');
            modals.forEach(function (trigger) {
              trigger.addEventListener('click', function (event) {
                event.preventDefault();
                const modal = document.getElementById(trigger.dataset.modal);
                document.getElementById('messageid').innerHTML = trigger.dataset.messageid;
                document.getElementById('date').innerHTML = trigger.dataset.date;
                document.getElementById('subject').innerHTML = trigger.dataset.subject;
                document.getElementById('from').innerHTML = trigger.dataset.from;
                document.getElementById('text').innerHTML = trigger.dataset.text;
                document.getElementById('html').innerHTML = trigger.dataset.html;
                document.getElementById('headers').innerHTML = trigger.dataset.headers;
                modal.classList.add('open');
                const exits = modal.querySelectorAll('.modal-exit');
                exits.forEach(function (exit) {
                  exit.addEventListener('click', function (event) {
                    event.preventDefault();
                    modal.classList.remove('open');
                  });
                });
              });
            });
        <?php if (isset($_GET['list']) && $_GET['list'] !== '') { ?>
            function findGetParameter(parameterName) {
                var result = null,
                    tmp = [];
                location.search
                    .substr(1)
                    .split("&")
                    .forEach(function (item) {
                      tmp = item.split("=");
                      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
                    });
                return result;
            }
            const mids = document.getElementsByClassName('message_id');
            const mid = document.getElementById('mid');
            mid.addEventListener(
                'input',
                (e) => {
                    e.preventDefault();
                    input = e.target.value;
                    for (let i = 0; i < mids.length; i++) {
                        if (mids[i].innerText.indexOf(input) == -1) {
                            mids[i].parentNode.setAttribute('style', 'display: none;')
                        } else {
                            mids[i].parentNode.setAttribute('style', '')
                        }
                    }
                }
            );
            message = findGetParameter('mid');
            if (message !== null) {
                mid.value = message;
                mid.dispatchEvent(new Event('input'));
            }
        <?php } else { ?>
            const lists = document.getElementById('lists').getElementsByTagName('a');
            document.getElementById('list').addEventListener(
                'input',
                (e) => {
                    input = e.target.value;
                    for (let i = 0; i < lists.length; i++) {
                        if (lists[i].innerText.indexOf(input) == -1) {
                            lists[i].parentNode.setAttribute('style', 'display: none;')
                        } else {
                            lists[i].parentNode.setAttribute('style', '')
                        }
                    }
                }
            );
        <?php } ?>
        </script>
    </body>
</html>
